.. LibreHealth Community documentation master file, created by
   sphinx-quickstart on Wed Mar 14 12:56:56 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

LibreHealth Community Documentation
===================================

Welcome to the LibreHealth documentation site. We are just getting started.

.. _Member Projects:
.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Member Projects

   toolkit/about


.. _Community Governance:
.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Community Governance Resources

   governance/governance
   governance/code-of-conduct
   governance/contribution-policy
   governance/copyright-dmca-policy
   governance/trademark-policy
   governance/privacy-policy


About this documentation
------------------------

We aim for this to be the one-stop for all documentation related to the LibreHealth community, as well as our member projects.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
