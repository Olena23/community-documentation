# Community Documentation site based on Sphinx using ReadTheDocs Theme

## Requirements

1. [pipenv](https://pipenv.readthedocs.io)
2. Python 3.7.x

## The documentation structure

1. All docs are to off of `docs/`
2. The path for a project should be `docs/projectname` (e.g., `docs/toolkit`)

## Building the docs

* On Linux, you can test your code by running `test-docs.sh`
* On Windows you can test your code using `make.bat`
